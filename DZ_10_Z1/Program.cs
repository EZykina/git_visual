﻿using System;

namespace DZ_4_Z2._1
{
    class Program
    {
        static void Main(string[] args)
        {/*Напишите программу русско-английский переводчик. Программа знает 10 слов о погоде. 
           Требуется, чтобы пользователь вводил слово на русском языке, а программа давала ему перевод этого слова 
           на английском языке. Если пользователь ввел слово, для которого отсутствует перевод, то следует вывести 
           сообщение, что такого слова нет.
          */
            Console.WriteLine("Вас приветствует русско-английский переводчик! \nВведите слово о погоде на русском языке с маленькой буквы: ");
            string Word = Console.ReadLine();
            Console.WriteLine();
            switch (Word)
            {
                case "солнечно":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("солнечно\t\tsunny");
                        break;
                    }
                case "пасмурно":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("пасмурно\t\tcloudy");
                        break;
                    }
                case "снег":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("снег\t\t\tsnow");
                        break;
                    }
                case "дождь":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("дождь\t\t\train");
                        break;
                    }
                case "град":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("град\t\t\thail");
                        break;
                    }
                case "гроза":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("гроза\t\t\tthunderstorm");
                        break;
                    }
                case "метель":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("метель\t\t\tsnowstorm");
                        break;
                    }
                case "ливень":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("ливень\t\t\trainfall");
                        break;
                    }
                case "ветер":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("ветер\t\t\twind");
                        break;
                    }
                case "мороз":
                    {
                        Console.WriteLine("Русский:\t\tАнглийский:");
                        Console.WriteLine("мороз\t\t\tfrost");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("\aИзвините, Вы ввели погодное явление, которого нет в нашем словаре, либо допустили ошибку при вводе. \nПроверьте введенное Вами слово и повторите попытку.");
                        break;
                    }
            }
            Console.WriteLine();
            Console.WriteLine("До новых встреч!\nХорошей Вам погоды!");
            Console.ReadKey();
        }
    }
}
